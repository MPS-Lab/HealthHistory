
import business.control.MedicoControle;
import business.model.Medico;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import exceptions.BDException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Alisson
 */
public class CriaMedicoDuplicado {
    
    private static MedicoControle medicoControle;
    private static Medico medico;
    
    @BeforeClass
    public static void setUpClass() {
        
        medicoControle = new MedicoControle();
        
        medico = new Medico("45879658951", "alissonmedico", "password123", "Alisson", "Galiza",
                "17/05/1996", "alissonmedico@alisson.com", "86998745632", "178238126361283861", "Cadiologista");
        
        medicoControle.remover("45879658951");
        medicoControle.adiciona(medico.getLogin(), medico);
    }
    
    @Test
    public void criaMedicoDuplicado(){
        Assert.assertFalse(medicoControle.adiciona(medico.getLogin(), medico));
        
    }
    
    @After
    public void tearDown(){
        medicoControle.remover("45879658951");
    }
}