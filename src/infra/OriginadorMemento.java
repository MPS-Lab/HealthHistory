package infra;

public class OriginadorMemento {
    
    private String estado;

    public void setState(String estado) {
        System.out.println("Originador setando estado: " + estado);
        this.estado = estado;
    }

    public Memento save() {
        System.out.println("Originator salvando memento.");
        return new Memento(estado);
    }
    public void restore(Memento m) {
        estado = m.getEstado();
        System.out.println("Originador retornando ao estado: " + estado);
    }
}