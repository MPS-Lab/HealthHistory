package exceptions;

/**
 *
 * @author Gabriel
 */
public class BDException extends Exception{
    
    public BDException(){
        super("Erro na Manipulação na Base de Dados");
    }
    
    public BDException(String exception){
        super(exception);
    }
    
}
