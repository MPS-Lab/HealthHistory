package business.gerencia;

/**
 *
 * @author Health History
 */
public class GeradorRelatorio{

    RelatorioTemplate relatorio;

    public GeradorRelatorio(RelatorioTemplate templateRelatorio) {
        this.relatorio = templateRelatorio;
    }
    
    
    protected void gerarRelatorio(String entidade){
        System.out.println(relatorio.gerarRelatorio(entidade));
        
        relatorio.salvarRelatorio();
        
    }
}