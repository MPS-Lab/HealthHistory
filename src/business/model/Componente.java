package business.model;

import java.util.Date;
/**
 *
 * @author Health History
 */
public abstract class Componente implements Comparable<Componente>{

    
    private String idComponente; 
    private Date dataCadastro;
    private String nome;
    private String descricao ;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricoo() {
        return descricao;
    }

    public void setDescricao(String descrição) {
        this.descricao = descricao;
    }

    public String getIdComponente() {
        return idComponente;
    }

    public void setIdComponente(String nomeComponente) {
        this.idComponente = nomeComponente;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }


    public Componente(String idComponente, String nome, Date dataCadastro, String descricao) {
        this.idComponente = idComponente;
        this.nome = nome;
        this.dataCadastro = dataCadastro;
        this.descricao = descricao;
    }

    @Override
    public int compareTo(Componente o) {
        return 1;
    }
    
    @Override
    
    public String toString() {
    
    return "Nome Componente: " +nome+ "Data de Cadastro: " +dataCadastro; 
    } 
 
}
