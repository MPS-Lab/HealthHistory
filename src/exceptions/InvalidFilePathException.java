package exceptions;

import java.io.File;

/**
 *
 * @author Health History
 */
public class InvalidFilePathException extends Exception{

    public InvalidFilePathException(File file) {
        super(file.getAbsolutePath() + " não contém um arquivo");
    }

    public InvalidFilePathException(String string) {
        super(string);
    }
    
    
    
}
