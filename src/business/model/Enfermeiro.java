package business.model;

/**
 *
 * @author HealthHistory
 */
public class Enfermeiro extends Usuario{
    
    private int cre; 

    public Enfermeiro(String login, String senha,String nome
            , String sobrenome,String cpf,String dataNascimento,String email
            , String telefones,int cre) {
        super(login, senha, nome, sobrenome, cpf, dataNascimento, email, telefones);
        this.cre = cre;
        
        
    }

    public int getCre() {
        return cre;
    }

    public void setCre(int cre) {
        this.cre = cre;
    }
    
}