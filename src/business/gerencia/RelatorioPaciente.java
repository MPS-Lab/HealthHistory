package business.gerencia;
import business.model.Paciente;
import exceptions.BDException;
import infra.DAO_Paciente;
import java.util.HashMap;
import java.util.Map;
import infra.PersistenciaArquivo;
import java.io.IOException;
/**
 *
 * @author Health History
 */
public class RelatorioPaciente extends RelatorioTemplate{
    
    @Override
    protected String getDados() {
        
        DAO_Paciente usuariosCadastrados = new DAO_Paciente();
        
        Map<String, Paciente> usuarios = new HashMap();
        try {
            usuarios =(HashMap) usuariosCadastrados.buscarTodos();
        } catch (BDException | IOException | ClassNotFoundException ex) {
            System.err.println(ex.getMessage());
        }
        
        String retorno = "";
        for (String key : usuarios.keySet()) {

            retorno += usuarios.get(key).toString() + "\n";
        }
        
        return      ">>> Relação de usuários\n"
                +   retorno;
            
    }
    
    @Override
    protected void salvarRelatorio() {
        
        PersistenciaArquivo arquivo = PersistenciaArquivo.getFileMager();
        arquivo.salvarArquivo("relatorioUsuarios.txt", this.gerarRelatorio("Usuarios"));
        
    }
    
}
