package infra;
import business.model.Medico;
import exceptions.BDException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author lumo
 */
public class DAO_Medico implements DatabaseManager{
    
    private Connection con = null; 
    
    public DAO_Medico() {

        con = ConnectionFactory.getConnection();

    }

    @Override
    public void adicionar(Object objeto) throws BDException{
        
        con = ConnectionFactory.getConnection();
        Medico medico = (Medico) objeto;

        String sql = "INSERT INTO Usuario (cpfUsuar, logiUsuar, senhUsuar, nomeUsuar, "
            + "sobrUsuar, dataUsuar,  emaiUsuar,  teleUsuar) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, medico.getCpf());
            stmt.setString(2, medico.getLogin());
            stmt.setString(3, medico.getSenha());
            stmt.setString(4, medico.getNome());
            stmt.setString(5, medico.getSobrenome());
            stmt.setString(6, medico.getDataNascimento());
            stmt.setString(7, medico.getEmail());
            stmt.setString(8, medico.getTelefone());
            
            stmt.executeUpdate();
            
            adicionarDuplicata(medico);
        } catch(com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException ex){
            throw new BDException("Usuário já existe. Criando uma especialização para Medico");
        } catch (SQLException ex) {
            throw new BDException();

        }finally {
            ConnectionFactory.closeConnection(con, stmt);
        }        
    }
    
    @Override
    public void alterar(Object objeto) throws BDException{

        con = ConnectionFactory.getConnection();
        Medico medico = (Medico) objeto;
        String sql = "UPDATE Medico SET cpfUsuar = ?, logiUsuar = ?, senhUsuar = ?, nomeUsuar = ?, "
                + "sobrUsuar = ?, dataUsuar = ?,  emaiUsuar = ?, teleUsuar = ?"
            + "WHERE cpfUsuar = ?";
        String sql2 = "UPDATE Medico SET crmMedic = ?, espeMedic = ? WHERE Usuario_cpfUsuar = ?";    
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, medico.getCpf());
            stmt.setString(2, medico.getLogin());
            stmt.setString(3, medico.getSenha());
            stmt.setString(4, medico.getNome());
            stmt.setString(5, medico.getSobrenome());
            stmt.setString(6, medico.getDataNascimento());
            stmt.setString(7, medico.getEmail());
            stmt.setString(8, medico.getTelefone());
            
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.err.println("Erro:" + ex);
            throw new BDException();
        } try {
            stmt = con.prepareStatement(sql2);
            stmt.setString(1, medico.getCpf());
            stmt.setString(2, medico.getCrm());
            stmt.setString(3, medico.getEspecialidade());
            stmt.executeUpdate();
        }
        catch (SQLException ex) {        
            System.err.println("Erro:" +ex);                
            throw new BDException();

        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }

    }
    
    @Override
    public Map<Object,Object> buscarTodos () throws BDException, IOException, ClassNotFoundException{
    
        con = ConnectionFactory.getConnection();
        String sql = "SELECT cpfUsuar, logiUsuar, senhUsuar, nomeUsuar, "
                + "sobrUsuar, dataUsuar, emaiUsuar , teleUsuar, crmMedic, espeMedic "
                + "FROM Usuario INNER JOIN Medico ON cpfUsuar = Usuario_cpfUsuar";

        PreparedStatement stmt = null;
        ResultSet rs = null;
        Map<Object,Object> medicos = new HashMap();

        try {

            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next()) {
                medicos.put(rs.getString("cpfUsuar"),
                        new Medico(
                        rs.getString("cpfUsuar"),
                        rs.getString("logiUsuar"),
                        rs.getString("senhUsuar"),
                        rs.getString("nomeUsuar"),
                        rs.getString("sobrUsuar"),
                        rs.getString("dataUsuar"),
                        rs.getString("emaiUsuar"),
                        rs.getString("teleUsuar"),
                        rs.getString("crmMedic"),
                        rs.getString("espeMedic")));
            }

            return medicos;

        } catch (SQLException ex) {
            System.err.println("Erro:" + ex);
            throw new BDException();
        } finally {
            ConnectionFactory.closeConnection(con, stmt, rs);
        }

    }
    
    @Override
    public Medico buscarChave (Object objeto) throws BDException{
        
        con = ConnectionFactory.getConnection();
        String cpfUsuar = (String) objeto;
        String sql = "SELECT cpfUsuar, logiUsuar, senhUsuar , nomeUsuar, "
                + "sobrUsuar, dataUsuar, emaiUsuar , teleUsuar, crmMedic, espeMedic "
                + "FROM Usuario INNER JOIN Medico ON cpfUsuar = Usuario_cpfUsuar";
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Medico medico = null;

        try {

            stmt = con.prepareStatement(sql);
            stmt.setString(1, cpfUsuar);
            
            rs = stmt.executeQuery();
            if (rs.next()) {
        medico = new Medico(
                        rs.getString("cpfUsuar"),
                        rs.getString("logiUsuar"),
                        rs.getString("senhUsuar"),
                        rs.getString("nomeUsuar"),
                        rs.getString("sobrUsuar"),
                        rs.getString("dataUsuar"),
                        rs.getString("emaiUsuar"),
                        rs.getString("teleUsuar"),
                        rs.getString("crmMedic"),
                        rs.getString("espeMedic"));
            }

            return medico;

        } catch (SQLException ex) {
            throw new BDException();
        } finally {
            ConnectionFactory.closeConnection(con, stmt, rs);
        }
    
    }
    
    @Override
    public void remover(Object objeto) throws BDException{
        con = ConnectionFactory.getConnection();
        
        String cpf = (String) objeto;
        String sql1 = "DELETE FROM Medico WHERE Usuario_cpfUsuar = "+cpf+"";
        String sql2 = "DELETE FROM Usuario WHERE cpfUsuar = "+cpf+"";

        PreparedStatement stmt = null;
        
        try {
            
            stmt = con.prepareStatement(sql1);
            stmt.execute();
        } catch (SQLException ex) {
            throw new BDException();
        }
        
        try {
            
            stmt = con.prepareStatement(sql2);
            stmt.execute();
        } catch (SQLException ex) {
            throw new BDException();
        }
        finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
    }
    
    /**
     * Metodo apenas para ser usado em testes. Nunca usar no banco real.
     * @throws util.exceptions.BDException
     */

    @Override
    public void removerTodos ()throws BDException {

    }

    @Override
    public void adicionarDuplicata(Object objeto) throws BDException {
        con = ConnectionFactory.getConnection();

        Medico medico = (Medico) objeto;             
        String sql = "INSERT INTO Medico (Usuario_cpfUsuar, crmMedic, espeMedic) VALUES (?,?,?)";
    
        PreparedStatement stmt = null; 
    
        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, medico.getCpf());
            stmt.setString(2, medico.getCrm());
            stmt.setString(3, medico.getEspecialidade());
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            throw new BDException();
        
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        } 
    }
    

   }