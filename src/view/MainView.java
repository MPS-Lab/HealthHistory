package view;

import java.util.Scanner;

/**
 *
 * @author Health History
 */
public class MainView {
    
    //singleton
    private static Fachada fachada;
    
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        fachada = Fachada.getFachada();
        exibeMenu();
            
    }
    
    private static void exibeMenu(){
        
        Scanner scan = new Scanner(System.in);
        boolean loop = true;            
        int opcao;
        
        while (loop) {
            System.out.println("\n"
                +          "Sou um Paciente          - 1 \n"
                +          "Sou um Médico            - 2 \n"
                +          "Sou um Enfermeiro        - 3 \n"
                +          "Gerar Relatorio Paciente - 4 \n"
                +          "Atualizar banco CID-10   - 5 \n"
                +          "Desfazer Ação Paciente   - 6 \n"    
                +          "Sair                     - 7");
            opcao = scan.nextInt();
            
            fachada.acoesDaFachada(opcao);
        }
    }

}
