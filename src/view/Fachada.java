package view;

import exceptions.NotAValidNumber;
import java.util.Scanner;


public class Fachada {
    
    Scanner scan = new Scanner(System.in);
    private static Fachada fachada;
    

    private Fachada() {
        //Singleton
    }
    
    public static Fachada getFachada(){
        if( fachada == null ){
            fachada = new Fachada();
        }
        return fachada;
    }
    
    public void acoesDaFachada(int opcao){
        Command command;
        
        switch(opcao){
            case 1:
                command = new ManipulaUsuario(opcao);
                break;
                
            case 2:
                command = new ManipulaUsuario(opcao);
                break;
                
            case 3:
                command = new ManipulaUsuario(opcao);
                break;
                
            case 4:
                command = new GeraRelatorioPaciente();
                break;
                
            case 5:
                command = new AtualizaBancoCID();
                break;
                
            case 6:
                command = new DesfazerAcao();
                break;
                
            case 7:
                System.exit(0);
                
            default:
                try {
                throw new NotAValidNumber();
                } catch (NotAValidNumber ex) {
                    System.err.println(ex.getMessage());
                    return;
                }
        
        }
        
        command.execute();
    }
}

    
