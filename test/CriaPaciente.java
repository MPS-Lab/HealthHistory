
import business.control.PacienteControle;
import business.model.Paciente;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Alisson
 */
public class CriaPaciente {
    private static PacienteControle pacienteControle;
    private static Paciente paciente;

    public CriaPaciente() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
        pacienteControle = new PacienteControle();
        
        paciente = new Paciente("45879658952", "alissonpaciente", "password123", "Alisson", "Galiza",
                "17/05/1996", "alissonpaciente@alisson.com", "86998745632", "ab+", "Rinite");
        
        pacienteControle.remover("45879658952");
    }
        
    
    @Test
    public void criaPaciente(){
        Assert.assertTrue(pacienteControle.adiciona(paciente.getLogin(), paciente));
        
    }
    
    @After
    public void tearDown(){
        
        pacienteControle.remover("45879658952");
    }
    
    
    
}
