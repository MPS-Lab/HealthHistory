package exceptions;

/**
 *
 * @author HealthHistory
 */
public class BuscaException extends Exception{

    public BuscaException(String message) {
        super(message);
    }
}