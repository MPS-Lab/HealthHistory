package infra;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HealthHistory
 */
public class PersistenciaArquivo {
    
    // singleton
    private static final PersistenciaArquivo ARQUIVO = new PersistenciaArquivo();
            
    public static synchronized PersistenciaArquivo getFileMager(){
        return ARQUIVO;
    }
    
    private PersistenciaArquivo() {
    }

    public List loadArquivo(String arquivoPath){
        List text = new ArrayList<>();
        Scanner p = new Scanner(System.in);
        do{
            try{
                BufferedReader br = new BufferedReader(new FileReader(arquivoPath));
                while(br.ready()){
                    text.add(br.readLine());
                }
             br.close();
             return text;
            }catch(IOException ioe){
                System.err.println("File not found");
                ioe.printStackTrace();
            }
           
        }while(true);
    }
    
    public void salvarArquivo(String arquivoPath, String conteudo){
        FileWriter arq;
        try {
            arq = new FileWriter(arquivoPath);
        
            PrintWriter gravarArq = new PrintWriter(arq);
            gravarArq.printf(conteudo);
            arq.close();
        } catch (IOException ex) {
            System.err.println("Cannot save file");
            ex.printStackTrace();
        }
        
    }
    
    public void salvarObjeto(String arquivo,Object objeto) throws IOException{
        File file = new File(arquivo);
        
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException ex) {
            System.err.println(ex.getMessage());
                throw new IOException("Erro ao criar arquivo de persistencia");
            }
        }
        
        try (ObjectOutputStream objectOut = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)))) {
            objectOut.writeObject(objeto);
        }
    }    

    public Object lerObjeto(String arquivo) throws IOException, ClassNotFoundException{
        File file = new File(arquivo);
        
        if (!file.exists()){
            throw new IOException("Arquivo nao encontrado");
        }
        
        ObjectInputStream objectIn = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)));
        try {
            return objectIn.readObject();
        } catch (ClassNotFoundException ex) {
            System.err.println(ex.getMessage());
            throw new ClassNotFoundException("ERRO: Erro de classe ao ler o arquivo");
        }finally{
            objectIn.close();
        }
        
    }
    
}
