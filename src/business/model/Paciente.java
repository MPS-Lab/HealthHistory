package business.model;


/**
 *
 * @author Health History
 */
public class Paciente extends Usuario{
    

    private String tipoSanguineo;
    private String doencas; 

    public Paciente(String cpf, String login, String senha, String nome, String sobrenome
            , String dataNascimento, String email, String telefone, String tipoSanguineo, String doencas) {
        super(cpf, login, senha, nome, sobrenome,  dataNascimento, email, telefone);
        this.tipoSanguineo = tipoSanguineo;
        this.doencas = doencas;
    }

    public String getDoencas() {
        return doencas;
    }

    public void setDoencas(String doencas) {
        this.doencas = doencas;
    }

    public String getTipoSanguineo() {
        return tipoSanguineo;
    }

    public void setTipoSanguineo(String tipoSanguineo) {
        this.tipoSanguineo = tipoSanguineo;
    }   
    
  }
    

