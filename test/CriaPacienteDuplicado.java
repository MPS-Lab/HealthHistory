
import business.control.PacienteControle;
import business.model.Paciente;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import exceptions.BDException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Alisson
 */
public class CriaPacienteDuplicado {
    
    private static PacienteControle pacienteControle;
    private static Paciente paciente;
    
    @BeforeClass
    public static void setUpClass() {
        
        pacienteControle = new PacienteControle();
        
        paciente = new Paciente("45879658952", "alissonpaciente", "password123", "Alisson", "Galiza",
                "17/05/1996", "alissonpaciente@alisson.com", "86998745632", "ab+", "Rinite");
        
        pacienteControle.remover("45879658952");
        pacienteControle.adiciona(paciente.getLogin(), paciente);
    }
    
    @Test
    public void criaPacienteDuplicado(){
        Assert.assertFalse(pacienteControle.adiciona(paciente.getLogin(), paciente));
        
    }
    
    @After
    public void tearDown(){
        pacienteControle.remover("45879658952");
    }
}


