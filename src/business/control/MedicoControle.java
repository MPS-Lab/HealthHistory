package business.control;
import business.model.Medico;
import business.model.Paciente;
import exceptions.*;
import infra.DAO_Medico;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HealthHistory
 */
public class MedicoControle extends UsuarioControle{
   
    HashMap<String, Medico> lista;
    DAO_Medico persistencia;
    
    public MedicoControle() {
        this.lista = new HashMap<>();
        persistencia = new DAO_Medico();
    }
    
    
    @Override
    public boolean validaLogin(String login) throws LoginException {
         
        if (login == null || login.length() == 0){
            throw new LoginException("Login não pode estar vazio");
        
        }else if (login.length()> 15){
            throw new LoginException("Número máximo de caracteres excedido (15 caracteres máx)");
        
        }else if(!this.containsLetter(login)){
            throw new LoginException("Login deve conter letras");
        }
        return true;
    }

    @Override
    public boolean validaSenha(String senha) throws SenhaException {
        
        if(senha == null || senha.length() == 0){
            throw new SenhaException("Senha não pode estar vazia");
        }

        if (senha.length() > 16){
            throw new SenhaException("Senha deve conter no máximo 16 caracteres");
            
            
        } else if (senha.length() < 8){
            throw  new SenhaException("Senha deve conter ao menos 8 digitos");
            
        } else if(!senha.matches(".*[0-9].*[0-9].*")){
            throw  new SenhaException("Senha deve conter ao menos 2 números");
            
        } else if(!this.containsLetter(senha)){
            throw new SenhaException("Senha deve conter letras");
        }
        return true;
    }

    @Override
    public boolean adiciona(String login, Object obj) {
        try {
            persistencia.adicionar(obj);
        } catch (BDException ex) {
            System.err.println(ex.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public boolean remover(String login) {
        try {
            persistencia.remover(login);
        } catch (BDException ex) {
            System.err.println(ex.getMessage());
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        String loginSenha = "";
        Medico medico;
        
        if(this.lista.isEmpty()){
            return "Nenhum Medico cadastrado";
        }
        
        for (String aux : this.lista.keySet()){
            medico = this.lista.get(aux);
            
            loginSenha += "Login = "+ medico.getLogin()+"     "
                    + "Senha = "+medico.getSenha()+"\n";
        }
        
        return "Lista de usuários cadastrados \n"
                + loginSenha;
    }  
    
    @Override
    public boolean buscarTodos() {
        try {
            lista = (HashMap)persistencia.buscarTodos();
        } catch (BDException | IOException | ClassNotFoundException ex) {
            System.err.println(ex.getMessage());
            return false;
        }
        return true;
    }
    
    @Override
    public String buscarIndividual(String cpf){
        
        Medico p;
        try {
            p = persistencia.buscarChave(cpf);
            if(p != null){
                return "\n" + p.getNome();
            }
            else{
                return "Nenhum Medico com esse CPF";
            }
        } catch (BDException ex) {
            System.err.println(ex.getMessage());
        }        
        return "\nUsuário não encontrado\n";
    }

    @Override
    public boolean alterar(Object obj) {
        try {
            persistencia.alterar(obj);
        } catch (BDException ex) {
            System.err.println(ex.getMessage());
            return false;
        }
        return true;
    }
    
    @Override
    public boolean validaTipoSanguineo(String tipoSanguineo) throws TipoSanguineoException {
        if(tipoSanguineo.length() > 3 ){
                throw new TipoSanguineoException();
        }
        return true;
    }
    
    @Override
    public boolean validaCPF(String cpf) throws CPFException {
            if(cpf.length() > 11){
                throw new CPFException();
            }
            if(this.containsLetter(cpf)){
                throw new CPFException("Cpf deve conter apenas Dígitos!!");
            }    
            return true;
    }
}
