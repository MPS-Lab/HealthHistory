package business.model;

import java.util.Date;

/**
 *
 * @author Health History
 */
public class Doenca extends Componente{
    
    private String descricao;
    
    public Doenca(String idComponente, Date dataCadastro, String nome, String descricao) {
        super(idComponente, nome, dataCadastro, descricao);
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    
    
}
