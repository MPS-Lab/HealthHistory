package exceptions;

/**
 *
 * @author HealthHistory
 */
public class LoginException extends Exception{

    public LoginException(String message) {
        super(message);
    }
}