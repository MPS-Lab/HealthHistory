import business.control.EnfermeiroControle;
import business.model.Enfermeiro;
import infra.ConnectionFactory;
import java.sql.Connection;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class CriaEnfermeiro {

    private static EnfermeiroControle enfermeiroControle;
    private static Enfermeiro enfermeiro;

    public CriaEnfermeiro() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
        enfermeiroControle = new EnfermeiroControle();
        
        enfermeiro = new Enfermeiro("45879658950", "alissonenfermeiro", "password123", "Alisson", "Galiza",
                "17/05/1996", "alissonenfermeiro@alisson.com", "86998745632", 123);
        
        enfermeiroControle.remover("45879658950");
    }
    
    @Test
    public void criaEnfermeiro(){
        Assert.assertTrue(enfermeiroControle.adiciona(enfermeiro.getLogin(), enfermeiro));
        
    }
    
    @After
    public void tearDown(){
        
        enfermeiroControle.remover("45879658950");
    }
}
