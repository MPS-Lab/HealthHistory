/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import infra.AtualizaCid;

/**
 *
 * @author lumo
 */
class AtualizaBancoCID implements Command {

    public AtualizaBancoCID() {
    }

    @Override
    public void execute() {
        System.out.println("Iniciando atualização do Cadastro Internacional de Doenças:\n");
        AtualizaCid cid = new AtualizaCid();
        cid.atualizar();
    }
    
    
}
