package exceptions;

/**
 * @author Health History
 */
public class ComponentMaxCharactersException extends Exception{

    public ComponentMaxCharactersException(String message) {
        super(message);
    }

    public ComponentMaxCharactersException() {
        super("Número máximo de caracteres excedido (128 caracteres máx)");
    }
        
    
}
