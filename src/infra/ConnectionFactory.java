package infra;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Rhenan Castelo Branco
 */
public class ConnectionFactory {
    
    //utilize essas variaveis para mudar a conexão
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static String URL;
    
    private static final String USER = "root";
    private static final String PASSWORD = "";
    
    
    public static Connection getConnection() {
        
        if(System.getProperty("os.name").equals("Linux")){
            URL = "jdbc:mysql://localhost:3306/HealthHistory?autoReconnect=true&useSSL=false"; 
        }else{
            URL = "jdbc:mysql://localhost:3306/HealthHistory";
        }
        
        try {
            Class.forName(DRIVER);
            return DriverManager.getConnection(URL, USER, PASSWORD);
            
        } catch (ClassNotFoundException ex) {
            System.err.println("Classe nao encontrada. Verifique se a biblioteca"
                    + "JBDC foi adicionada");
            System.err.println(ex.getMessage());
            ex.printStackTrace();
            System.exit(0);
        }catch(SQLException ex){
                        throw new RuntimeException("Connection Error", ex);
            
        }
        
        return null;
    }
    
    public static void closeConnection(Connection con) {
        
        if (con != null) {
            try {
                con.close();
            } catch (SQLException ex) {
                System.err.println("Error:" +ex);
            }
        }
    }

    public static void closeConnection(Connection con, java.sql.PreparedStatement stmt) {
        
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                System.err.println("Error:" +ex);
            }
        }
        closeConnection(con);
    }
    
    public static void closeConnection(Connection con, java.sql.PreparedStatement stmt, ResultSet rs) {
        
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ex) {
                System.err.println("Error:" +ex);
            }
        }
        closeConnection(con,stmt);
    }
    
}
