/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package infra;
import java.io.IOException;
import java.util.Map;
import exceptions.BDException;

/**
 * Interface de persistencia. <br>
 * Classes que implementarem essa interface devel salvar um objeto em uma base de dados 
 * @author Gabriel
 */

public interface Persistencia {
    
    /**
     * Retorna um map de todos os objetos de alguma entidade da base de dados
     * @return Map(String, Produto), onde a chave é o codigo de barras do produto (chave primaria)
     * @throws java.io.IOException
     * @throws java.lang.ClassNotFoundException
     */
    public Map<Object, Object> buscarTodos() throws BDException, IOException, ClassNotFoundException;
    
    /**
     * Insere um objecto em alguma entidade da base de dados
     * @param objeto(Object), objeto a ser inserido na base de dados
     * @throws util.exceptions.BDException,Erro na manipulação do banco
     * @return boleano indicando se a operação foi bem sucedida
     */
    public void adicionar(Object objeto) throws BDException;
    
    /**
     * Busca um objeto contido em alguma entidade da base de dados através de sua
     * chave primária
     * @param objeto(Object), chave a ser buscada
     * @return (Object,null), Objecto encontrado ou valor nulo caso contrário
     * @throws util.exceptions.BDException,Erro na manipulação do banco
     */
    public Object buscarChave(Object objeto) throws BDException;
   
    /**
     * Remove um registro da base de dados de acordo com parametro passado
     * @param objeto(Object), objeto a ser removido
     * @throws util.exceptions.BDException,Erro na manipulação do banco
     * @return boleano indicando se a operação foi bem sucedida
     */
    public void remover(Object objeto) throws BDException;
    
    /**
     * Remove todos os registros de uma entidade da base de dados 
     * @throws util.exceptions.BDException,Erro na manipulação do banco
     */
    public void removerTodos() throws BDException;
    
    /**
     * Altera algum registro da base de dados de acordo com chave passada
     * @param objeto(Object), parametro buscado e atualizado
     * @throws util.exceptions.BDException,Erro na manipulação do banco
     * @return boleano indicando se a operação foi bem sucedida
     */
    public void alterar(Object objeto) throws BDException;
    
    
    public void adicionarDuplicata (Object objeto) throws BDException; 
}
