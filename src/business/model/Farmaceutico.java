package business.model;

/**
 *
 * @author HealthHistory
 */
public class Farmaceutico extends Usuario{
    
    private String crf; 

    public Farmaceutico(String login, String senha,String nome
            , String sobrenome,String cpf,String dataNascimento,String email
            , String telefone,String crf) {
        super(login, senha, nome, sobrenome, cpf, dataNascimento, email, telefone);
        this.crf = crf;
        
        
    }

    public String getCrf() {
        return crf;
    }

    public void setCrf(String crf) {
        this.crf = crf;
    }
    
}