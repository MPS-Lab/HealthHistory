package infra;

import business.model.Paciente;
import exceptions.BDException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Rhenan Castelo Branco
 */

public class DAO_Paciente implements DatabaseManager{
    
    private Connection con = null; 
    
    public DAO_Paciente() {

    }

    @Override
    public void adicionarDuplicata (Object objeto) throws BDException {
         con = ConnectionFactory.getConnection();

        Paciente paciente = (Paciente) objeto;             
        String sql = "INSERT INTO Paciente (Usuario_cpfUsuar, tsanPacie, doenPacie) VALUES (?,?,?)" ;
    
        PreparedStatement stmt = null; 
    
        try {

            stmt = con.prepareStatement(sql);
            stmt.setString(1, paciente.getCpf());
            stmt.setString(2, paciente.getTipoSanguineo());
            stmt.setString(3, paciente.getDoencas());
            
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.err.println("Erro PACIENTE:" + ex);
            throw new BDException();
        
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
    
    }
    @Override
    public void adicionar(Object objeto) throws BDException{
        con = ConnectionFactory.getConnection();

        Paciente paciente = (Paciente) objeto;
        
        String sql = "INSERT INTO Usuario (cpfUsuar, logiUsuar, senhUsuar, nomeUsuar, "
            + "sobrUsuar, dataUsuar,  emaiUsuar,  teleUsuar) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

        PreparedStatement stmt = null;

        try {

            stmt = con.prepareStatement(sql);
            stmt.setString(1, paciente.getCpf());
            stmt.setString(2, paciente.getLogin());
            stmt.setString(3, paciente.getSenha());
            stmt.setString(4, paciente.getNome());
            stmt.setString(5, paciente.getSobrenome());
            stmt.setString(6, paciente.getDataNascimento());
            stmt.setString(7, paciente.getEmail());
            stmt.setString(8, paciente.getTelefone());


            stmt.executeUpdate();

            adicionarDuplicata(paciente);
        } catch(com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException ex){
            throw new BDException("Usuário já existe. Criando uma especialização para Enfermeiro");
        } catch (SQLException ex) {
            throw new BDException();

        }finally {
            ConnectionFactory.closeConnection(con, stmt);
        }        

    }
    
    @Override
    public void alterar(Object objeto) throws BDException{
        con = ConnectionFactory.getConnection();    
        
        Paciente paciente = (Paciente) objeto;
        String sql = "UPDATE Usuario SET cpfUsuar = ?, logiUsuar = ?, senhUsuar = ?, nomeUsuar = ?, "
                + "sobrUsuar = ?, dataUsuar = ?,  emaiUsuar = ?, teleUsuar = ?"
            + "WHERE cpfUsuar = ?";
        String sql2  = "UPDATE Paciente SET tsanPacie = ? doenPacie = ? WHERE Usuario_cpfUsuar = ?";

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, paciente.getCpf());
            stmt.setString(2, paciente.getLogin());
            stmt.setString(3, paciente.getSenha());
            stmt.setString(4, paciente.getNome());
            stmt.setString(5, paciente.getSobrenome());
            stmt.setString(6, paciente.getDataNascimento());
            stmt.setString(7, paciente.getEmail());
            stmt.setString(8, paciente.getTelefone());
            stmt.executeUpdate();

        } catch (SQLException ex) {
            System.err.println("Erro:" + ex);
            throw new BDException();
        } try {

            stmt = con.prepareStatement(sql2);
            stmt.setString(1, paciente.getCpf());
            stmt.setString(2, paciente.getTipoSanguineo());
            stmt.setString(3, paciente.getDoencas());
            
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            throw new BDException();
        
        }
        
        finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
        
        

    }
    
    @Override
    public Map<Object,Object> buscarTodos () throws BDException, IOException, ClassNotFoundException{
        con = ConnectionFactory.getConnection();
    
        String sql = "SELECT cpfUsuar, logiUsuar, senhUsuar, nomeUsuar, "
                + "sobrUsuar, dataUsuar, emaiUsuar , teleUsuar, tsanPacie, doenPacie "
                + "FROM Usuario INNER JOIN Paciente ON cpfUsuar = Usuario_cpfUsuar"; 

        PreparedStatement stmt = null;
        ResultSet rs = null;
        Map<Object,Object> pacientes = new HashMap();

        try {

            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next()) {
                pacientes.put(rs.getString("cpfUsuar"),
                        new Paciente(
                        rs.getString("cpfUsuar"),
                        rs.getString("logiUsuar"),
                        rs.getString("senhUsuar"),
                        rs.getString("nomeUsuar"),
                        rs.getString("sobrUsuar"),
                        rs.getString("dataUsuar"),
                        rs.getString("emaiUsuar"),
                        rs.getString("teleUsuar"),
                        rs.getString("tsanPacie"),
                        rs.getString("doenPacie")));
            }

            return pacientes;

        } catch (SQLException ex) {
            throw new BDException();
        } finally {
            ConnectionFactory.closeConnection(con, stmt, rs);
        }


    }
    
    @Override
    public Paciente buscarChave (Object objeto) throws BDException{
        con = ConnectionFactory.getConnection();
        
        String cpfUsuar = (String) objeto;
        String sql = "SELECT cpfUsuar, logiUsuar, senhUsuar , nomeUsuar, "
                + "sobrUsuar, dataUsuar, emaiUsuar , teleUsuar, tsanPacie, doenPacie "
                + "FROM Usuario INNER JOIN Paciente ON cpfUsuar = Usuario_cpfUsuar WHERE cpfUsuar = ?";
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Paciente paciente = null;

        try {

            stmt = con.prepareStatement(sql);
            stmt.setString(1, cpfUsuar);
            
            rs = stmt.executeQuery();
            if (rs.next()) {
        paciente = new Paciente(
                        rs.getString("cpfUsuar"),
                        rs.getString("logiUsuar"),
                        rs.getString("senhUsuar"),
                        rs.getString("nomeUsuar"),
                        rs.getString("sobrUsuar"),
                        rs.getString("dataUsuar"),
                        rs.getString("emaiUsuar"),
                        rs.getString("teleUsuar"),
                        rs.getString("tsanPacie"),
                        rs.getString("doenPacie"));
            }

            return paciente;

        } catch (SQLException ex) {
            System.err.println("Erro:" + ex);
            throw new BDException();
        } finally {
            ConnectionFactory.closeConnection(con, stmt, rs);
        }
    
    }
    
    @Override
    public void remover(Object objeto) throws BDException{
        con = ConnectionFactory.getConnection();
        
        String cpf = (String) objeto;
        String sql1 = "DELETE FROM Paciente WHERE Usuario_cpfUsuar = "+cpf+"";
        String sql2 = "DELETE FROM Usuario WHERE cpfUsuar = "+cpf+"";

        PreparedStatement stmt = null;
        
        try {
            
            stmt = con.prepareStatement(sql1);
            stmt.execute();
        } catch (SQLException ex) {
            throw new BDException();
        }
        
        try {
            
            stmt = con.prepareStatement(sql2);
            stmt.execute();
        } catch (SQLException ex) {
            throw new BDException();
        }
        finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
        
        
    }
    
    /**
     * Metodo apenas para ser usado em testes. Nunca usar no banco real.
     * @throws util.exceptions.BDException
     */

    @Override
    public void removerTodos ()throws BDException {

    }
    

    
}
