package exceptions;

/**
 *
 * @author HealthHistory
 */
public class CPFException extends Exception{
    
    public CPFException(){
        super("CPF deve conter até 11 Digitos!!");
    }
    
    public CPFException(String exception){
        super(exception);
    }
    
}
