/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package infra;
import business.model.Enfermeiro;
import exceptions.BDException;
import infra.ConnectionFactory;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lumo
 */
public class DAO_Enfermeiro implements DatabaseManager{
    
    private Connection con = null; 
    
    public DAO_Enfermeiro() {

        con = ConnectionFactory.getConnection();

    }

    @Override
    public void adicionar(Object objeto) throws BDException{

        con = ConnectionFactory.getConnection();
        Enfermeiro enfermeiro = (Enfermeiro) objeto;

        String sql = "INSERT INTO Usuario (cpfUsuar, logiUsuar, senhUsuar, nomeUsuar, "
            + "sobrUsuar, dataUsuar,  emaiUsuar,  teleUsuar) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        

        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, enfermeiro.getCpf());
            stmt.setString(2, enfermeiro.getLogin());
            stmt.setString(3, enfermeiro.getSenha());
            stmt.setString(4, enfermeiro.getNome());
            stmt.setString(5, enfermeiro.getSobrenome());
            stmt.setString(6, enfermeiro.getDataNascimento());
            stmt.setString(7, enfermeiro.getEmail());
            stmt.setString(8, enfermeiro.getTelefone());
            
            stmt.executeUpdate();
            
            adicionarDuplicata(enfermeiro);
        
        } catch(com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException ex){
            throw new BDException("Usuário já existe. Criando uma especialização para Enfermeiro");
        } catch (SQLException ex) {
            throw new BDException();
        }finally {
            ConnectionFactory.closeConnection(con, stmt);
        }

    }
    
    @Override
    public void alterar(Object objeto) throws BDException{
        
        con = ConnectionFactory.getConnection();
        Enfermeiro enfermeiro = (Enfermeiro) objeto;
        String sql = "UPDATE Usuario SET cpfUsuar = ?, logiUsuar = ?, senhUsuar = ?, nomeUsuar = ?, "
                + "sobrUsuar = ?, dataUsuar = ?,  emaiUsuar = ?, teleUsuar = ?"
            + "WHERE cpfUsuar = ?";
        String sql2 = "UPDATE Enfermeiro SET creEnfer = ? WHERE Usuario_cpfUsuar = ?";    
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, enfermeiro.getCpf());
            stmt.setString(2, enfermeiro.getLogin());
            stmt.setString(3, enfermeiro.getSenha());
            stmt.setString(4, enfermeiro.getNome());
            stmt.setString(5, enfermeiro.getSobrenome());
            stmt.setString(6, enfermeiro.getDataNascimento());
            stmt.setString(7, enfermeiro.getEmail());
            stmt.setString(8, enfermeiro.getTelefone());
            
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.err.println("Erro:" + ex);
            throw new BDException();
        } try {
            stmt = con.prepareStatement(sql2);
            stmt.setString(1, enfermeiro.getCpf());
            stmt.setInt(2, enfermeiro.getCre());
            stmt.executeUpdate();
        }
        catch (SQLException ex) {        
            System.err.println("Erro:" +ex);                
            throw new BDException();

        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        }

    }
    
    @Override
    public Map<Object,Object> buscarTodos () throws BDException, IOException, ClassNotFoundException{
    
        con = ConnectionFactory.getConnection();
        String sql = "SELECT cpfUsuar, logiUsuar, senhUsuar, nomeUsuar, "
                + "sobrUsuar, dataUsuar, emaiUsuar , teleUsuar, creEnfer, especMedic "
                + "FROM Usuario INNER JOIN Enfermeiro ON cpfUsuar = Usuario_cpfUsuar";

        PreparedStatement stmt = null;
        ResultSet rs = null;
        Map<Object,Object> enfermeiros = new HashMap();

        try {

            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next()) {
                enfermeiros.put(rs.getString("cpfUsuar"),
                        new Enfermeiro(
                        rs.getString("cpfUsuar"),
                        rs.getString("logiUsuar"),
                        rs.getString("senhUsuar"),
                        rs.getString("nomeUsuar"),
                        rs.getString("sobrUsuar"),
                        rs.getString("dataUsuar"),
                        rs.getString("emaiUsuar"),
                        rs.getString("teleUsuar"),
                        rs.getInt("creEnfer")));
            }

            return enfermeiros;

        } catch (SQLException ex) {
            System.err.println("Erro:" + ex);
            throw new BDException();
        } finally {
            ConnectionFactory.closeConnection(con, stmt, rs);
        }

    }
    
    @Override
    public Enfermeiro buscarChave (Object objeto) throws BDException{
        
        con = ConnectionFactory.getConnection();
        String cpfUsuar = (String) objeto;
        String sql = "SELECT cpfUsuar, logiUsuar, senhUsuar , nomeUsuar, "
                + "sobrUsuar, dataUsuar, emaiUsuar , teleUsuar, creEnfer "
                + "FROM Usuario INNER JOIN Enfermeiro ON cpfUsuar = Usuario_cpfUsuar WHERE cpfUsuar = ?";
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Enfermeiro enfermeiro = null;

        try {

            stmt = con.prepareStatement(sql);
            stmt.setString(1,cpfUsuar);
            
            rs = stmt.executeQuery();
            if (rs.next()) {
        enfermeiro = new Enfermeiro(
                        rs.getString("cpfUsuar"),
                        rs.getString("logiUsuar"),
                        rs.getString("senhUsuar"),
                        rs.getString("nomeUsuar"),
                        rs.getString("sobrUsuar"),
                        rs.getString("dataUsuar"),
                        rs.getString("emaiUsuar"),
                        rs.getString("teleUsuar"),
                        rs.getInt("creEnfer"));
            }

            return enfermeiro;

        } catch (SQLException ex) {
            System.err.println("Erro:" + ex);
            throw new BDException();
        } finally {
            ConnectionFactory.closeConnection(con, stmt, rs);
        }
    
    }
    //NAO USAR JAMAIS
    @Override
    public void remover(Object objeto) throws BDException{
       con = ConnectionFactory.getConnection();
        
        String cpf = (String) objeto;
        String sql1 = "DELETE FROM Enfermeiro WHERE Usuario_cpfUsuar = "+cpf+"";
        String sql2 = "DELETE FROM Usuario WHERE cpfUsuar = "+cpf+"";

        PreparedStatement stmt = null;
        
        try {
            
            stmt = con.prepareStatement(sql1);
            stmt.execute();
        } catch (SQLException ex) {
            System.err.println("Erro:" + ex);
            throw new BDException();
        }
        
        try {
            
            stmt = con.prepareStatement(sql2);
            stmt.execute();
        } catch (SQLException ex) {
            System.err.println("Erro:" + ex);
            throw new BDException();
        }
        finally {
            ConnectionFactory.closeConnection(con, stmt);
        }
    }
    
    /**
     * Metodo apenas para ser usado em testes. Nunca usar no banco real.
     * @throws util.exceptions.BDException
     */

    @Override
    public void removerTodos ()throws BDException {

    }

   @Override
    public void adicionarDuplicata(Object objeto) throws BDException {
        con = ConnectionFactory.getConnection();

        Enfermeiro enfermeiro = (Enfermeiro) objeto;             
        String sql = "INSERT INTO Enfermeiro (Usuario_cpfUsuar, creEnfer) VALUES (?,?)";    
        
        PreparedStatement stmt = null; 
    
        try {
            stmt = con.prepareStatement(sql);
            stmt.setString(1, enfermeiro.getCpf());
            stmt.setInt(2, enfermeiro.getCre());
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.err.println("Erro Enfermeiro:" + ex);
            throw new BDException();
        
        } finally {
            ConnectionFactory.closeConnection(con, stmt);
        } 
    }
    

   }