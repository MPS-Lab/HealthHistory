package exceptions;

/**
 *
 * @author Health History
 */
public class LoginEmptyException extends Exception{

    public LoginEmptyException() {
        super("Login não pode estar vazio");
    }

    public LoginEmptyException(String message) {
        super(message);
    }
    
    
    
}
