import business.control.EnfermeiroControle;
import business.model.Enfermeiro;
import exceptions.BuscaException;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author lumo
 */
public class RemoveEnfermeiro {
    private static EnfermeiroControle enfermeiroControle;
    private static Enfermeiro enfermeiro;
    
    @BeforeClass
    public static void setUpClass() {
    
        
        enfermeiroControle = new EnfermeiroControle();
        
        enfermeiro = new Enfermeiro("45879658950", "alissonenfermeiro", "password123", "Alisson", "Galiza",
                "17/05/1996", "alissonenfermeiro@alisson.com", "86998745632", 123);
        enfermeiroControle.adiciona(enfermeiro.getLogin(), enfermeiro);
    }
    
    @Test
    public void RemoveEnfermeiro(){
        Assert.assertTrue(enfermeiroControle.remover("45879658950"));
        Assert.assertEquals("Nenhum Enfermeiro com esse CPF", enfermeiroControle.buscarIndividual("45879658951"));
    }
}
