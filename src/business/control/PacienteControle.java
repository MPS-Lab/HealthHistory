package business.control;
import business.model.Paciente;
import exceptions.*;
import infra.DAO_Paciente;
import infra.Memento;
import infra.OriginadorMemento;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author HealthHistory
 */
public class PacienteControle extends UsuarioControle{
   
    HashMap<String, Paciente> lista;
    private ArrayList<Memento> mementos;
    OriginadorMemento orig;
    DAO_Paciente persistencia;
    
    public PacienteControle() {
        this.lista = new HashMap<>();
        mementos = new ArrayList<>();
        orig = new OriginadorMemento();
        persistencia = new DAO_Paciente();
        orig.setState("Inicial");
        adicionaMemento(orig.save());
    }
    
    @Override
    public boolean validaTipoSanguineo(String tipoSanguineo) throws TipoSanguineoException {
        if(tipoSanguineo.length() > 3 ){
                throw new TipoSanguineoException();
        }
        return true;
    }    
    
    @Override
    public boolean validaCPF(String cpf) throws CPFException {
            if(cpf.length() > 11){
                throw new CPFException();
            }
            if(this.containsLetter(cpf)){
                throw new CPFException("Cpf deve conter apenas Dígitos!!");
            }    
            return true;
    }
    
    @Override
    public boolean validaLogin(String login) throws LoginException {
         
        if (login == null || login.length() == 0){
            throw new LoginException("Login não pode estar vazio");
        
        }else if (login.length()> 15){
            throw new LoginException("Número máximo de caracteres excedido (15 caracteres máx)");
        
        }else if(!this.containsLetter(login)){
            throw new LoginException("Login deve conter letras");
        }
        return true;
    }

    @Override
    public boolean validaSenha(String senha) throws SenhaException {
        
        if(senha == null || senha.length() == 0){
            throw new SenhaException("Senha não pode estar vazia");
        }

        if (senha.length() > 16){
            throw new SenhaException("Senha deve conter no máximo 16 caracteres");
            
            
        } else if (senha.length() < 8){
            throw  new SenhaException("Senha deve conter ao menos 8 digitos");
            
        } else if(!senha.matches(".*[0-9].*[0-9].*")){
            throw  new SenhaException("Senha deve conter ao menos 2 números");
            
        } else if(!this.containsLetter(senha)){
            throw new SenhaException("Senha deve conter letras");
        }
        return true;
    }

    @Override
    public boolean adiciona(String login, Object obj) {
        try {
            persistencia.adicionar(obj);
        } catch (BDException ex) {
            System.err.println(ex.getMessage());
            return false;
        }
        orig.setState("Inserção");
        mementos.add(orig.save());
        return true;
    }

    @Override
    public boolean remover(String login) {
        try {
            persistencia.remover(login);
        } catch (BDException ex) {
            System.err.println(ex.getMessage());
            return false;
        }
        orig.setState("Remoção");
        mementos.add(orig.save());
        return true;
    }
    
    @Override
    public String toString() {
        String loginSenha = "";
        Paciente paciente;
        
        buscarTodos();              //pesquisa no banco e atualiza a lista pra exibir
        
        if(this.lista.isEmpty()){
            return "Nenhum Paciente cadastrado";
        }
        
        for (String aux : this.lista.keySet()){
            paciente = this.lista.get(aux);
            
            loginSenha += "Login = "+ paciente.getLogin()+"     "
                    + "Senha = "+paciente.getSenha()+"\n";
        }
        
        return "Lista de Pacientes cadastrados \n"
                + loginSenha;
    }
    
    @Override
    public boolean buscarTodos() {
        try {
            lista = (HashMap)persistencia.buscarTodos();
        } catch (BDException | IOException | ClassNotFoundException ex) {
            System.err.println(ex.getMessage());
            return false;
        }
        return true;
    }
    
    @Override
    public String buscarIndividual(String cpf){
        
        Paciente p;
        try {
            p = persistencia.buscarChave(cpf);
            if(p != null){
                return "\n" + p.getNome();
            }
            else{
                return "Nenhum Paciente com esse CPF";
            }
        } catch (BDException ex) {
            System.err.println(ex.getMessage());
        }        
        return "\nUsuário não encontrado\n";
    }

    @Override
    public boolean alterar(Object obj) {
        try {
            persistencia.alterar(obj);
        } catch (BDException ex) {
            System.err.println(ex.getMessage());
            return false;
        }
        return true;
    }
    
    public void desfazerPaciente(){
        System.out.println("Desfazendo ação: "+getMemento());
        dropMemento();
        orig.restore(getMemento());
    }
    
    public void adicionaMemento(Memento m) {
        mementos.add(0,m);
    }

    public Memento getMemento() {
        return mementos.get(0);
    } 

    public void dropMemento(){
        mementos.remove(0);
    }    
}
