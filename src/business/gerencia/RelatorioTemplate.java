package business.gerencia;

import java.util.Date;

/**
 *
 * @author HealthHistory
 */
public abstract class RelatorioTemplate {
     
    protected String getTitulo(String entidade){
        return "=========SISTEMA HEALTH HISTORY=============\n"
                + "RELATORIO de "+ entidade
                + "\nData: "+new Date()+"\n";
    }
    
    protected String fecharRelatorio(){
        return "============================================\n";
    }
    
    protected abstract String getDados();
    
    public String gerarRelatorio(String entidade){
        return this.getTitulo(entidade)+this.getDados()+this.fecharRelatorio();
    }
    
    protected abstract void salvarRelatorio();
}
