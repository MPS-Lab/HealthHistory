//package business.control;
//import business.model.Farmaceutico;
//import exceptions.*;
//import infra.DAO_Farmaceutico;
//import java.io.IOException;
//import java.util.HashMap;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
///**
// *
// * @author HealthHistory
// */
//public class FarmaceuticoControle implements Controle{
//   
//    HashMap<String, Farmaceutico> lista;
//    DAO_Farmaceutico persistencia;
//    
//    public FarmaceuticoControle() {
//        this.lista = new HashMap<>();
//        persistencia = new DAO_Farmaceutico();
//    }
//    
//    
//    @Override
//    public boolean validaLogin(String login) throws LoginException {
//         
//        if (login == null || login.length() == 0){
//            throw new LoginException("Login não pode estar vazio");
//        
//        }else if (login.length()> 15){
//            throw new LoginException("Número máximo de caracteres excedido (15 caracteres máx)");
//        
//        }else if(!this.containsLetter(login)){
//            throw new LoginException("Login deve conter letras");
//        }
//        return true;
//    }
//
//    @Override
//    public boolean validaSenha(String senha) throws SenhaException {
//        
//        if(senha == null || senha.length() == 0){
//            throw new SenhaException("Senha não pode estar vazia");
//        }
//
//        if (senha.length() > 16){
//            throw new SenhaException("Senha deve conter no máximo 16 caracteres");
//            
//            
//        } else if (senha.length() < 8){
//            throw  new SenhaException("Senha deve conter ao menos 8 digitos");
//            
//        } else if(!senha.matches(".*[0-9].*[0-9].*")){
//            throw  new SenhaException("Senha deve conter ao menos 2 números");
//            
//        } else if(!this.containsLetter(senha)){
//            throw new SenhaException("Senha deve conter letras");
//        }
//        return true;
//    }
//
//    @Override
//    public boolean adiciona(String login, Object obj) {
//        try {
//            persistencia.adicionar(obj);
//        } catch (BDException ex) {
//            Logger.getLogger(FarmaceuticoControle.class.getName()).log(Level.SEVERE, null, ex);
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public boolean remover(String login) {
//        try {
//            persistencia.remover(login);
//        } catch (BDException ex) {
//            Logger.getLogger(FarmaceuticoControle.class.getName()).log(Level.SEVERE, null, ex);
//            return false;
//        }
//        return true;
//    }
//    
//    @Override
//    public String toString() {
//        String loginSenha = "";
//        Farmaceutico medico;
//        
//        if(this.lista.isEmpty()){
//            return "Nenhum Farmaceutico cadastrado";
//        }
//        
//        for (String aux : this.lista.keySet()){
//            medico = this.lista.get(aux);
//            
//            loginSenha += "Login = "+ medico.getLogin()+"     "
//                    + "Senha = "+medico.getSenha()+"\n";
//        }
//        
//        return "Lista de usuários cadastrados \n"
//                + loginSenha;
//    }  
//    
//    private boolean containsLetter(String str){
//        for (int i = 0; i<str.length();i++){
//            
//            if (Character.isLetter(str.charAt(i))){
//                return true;
//            }
//        }
//        
//        return false;
//    }
//
//    @Override
//    public boolean buscarTodos() {
//        try {
//            lista = (HashMap)persistencia.buscarTodos();
//        } catch (BDException | IOException | ClassNotFoundException ex) {
//            Logger.getLogger(FarmaceuticoControle.class.getName()).log(Level.SEVERE, null, ex);
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public boolean alterar(Object obj) {
//        try {
//            persistencia.alterar(obj);
//        } catch (BDException ex) {
//            Logger.getLogger(FarmaceuticoControle.class.getName()).log(Level.SEVERE, null, ex);
//            return false;
//        }
//        return true;
//    }
//}
