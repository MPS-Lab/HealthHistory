
package business.control;

import exceptions.CPFException;
import exceptions.LoginException;
import exceptions.SenhaException;
import exceptions.TipoSanguineoException;


/**
 *
 * @author Alisson
 */
public abstract class UsuarioControle {
    
    public abstract boolean validaLogin(String login) throws LoginException;

    public abstract boolean validaSenha(String senha)  throws SenhaException;

    public abstract boolean validaTipoSanguineo(String senha)  throws TipoSanguineoException;
    
    public abstract boolean validaCPF(String senha)  throws CPFException;
    
    public abstract boolean adiciona(String login, Object obj);

    public abstract boolean remover(String login);
    
    @Override
    public abstract String toString();
    
    public boolean containsLetter(String str){
        for (int i = 0; i<str.length();i++){
            
            if (Character.isLetter(str.charAt(i))){
                return true;
            }
        }
        
        return false;
    }

    public abstract boolean buscarTodos();
    
    public abstract String buscarIndividual(String cpf);

    public abstract boolean alterar(Object obj);
    
}
