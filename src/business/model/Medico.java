/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.model;

/**
 *
 * @author lumo
 */
public class Medico extends Usuario {
    
    String crm; 
    String especialidade; 

    public Medico(String cpf, String login, String senha, String nome, String sobrenome
            , String dataNascimento, String email, String telefone, String crm, String especialidade) {
        super(cpf, login, senha, nome, sobrenome, dataNascimento, email, telefone);
        this.crm = crm; 
        this.especialidade = especialidade;
    }

    public String getCrm() {
        return crm;
    }

    public void setCrm(String crm) {
        this.crm = crm;
    }

    public String getEspecialidade() {
        return especialidade;
    }

    public void setEspecialidade(String especialidade) {
        this.especialidade = especialidade;
    }
    
    
    
}
