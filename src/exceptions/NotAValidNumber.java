/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author Alisson
 */
public class NotAValidNumber extends Exception{
    
    public NotAValidNumber(){
        super("Número inválido. Selecione um dos números listados");
    }
    
    public NotAValidNumber(String exception){
        super(exception);
    }
}
