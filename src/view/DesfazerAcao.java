/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import business.control.PacienteControle;

/**
 *
 * @author lumo
 */
class DesfazerAcao implements Command {

    public DesfazerAcao() {
    }

    @Override
    public void execute() {
        PacienteControle pc = new PacienteControle();
        pc.desfazerPaciente();
    }
    
    
    
}
