
import business.control.MedicoControle;
import business.model.Medico;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
/**
 *
 * @author lumo
 */
public class CriaMedico {
    private static MedicoControle medicoControle;
    private static Medico medico;

    public CriaMedico() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
        medicoControle = new MedicoControle();
        
        medico = new Medico("45879658951", "alissonmedico", "password123", "Alisson", "Galiza",
                "17/05/1996", "alissonmedico@alisson.com", "86998745632", "178238126361283861", "Cadiologista");
        
        medicoControle.remover("45879658951");
    }
    
    @Test
    public void criaMedico(){
        Assert.assertTrue(medicoControle.adiciona(medico.getLogin(), medico));
        
    }
    
    @After
    public void tearDown(){
        
        medicoControle.remover("45879658951");
    }

}
