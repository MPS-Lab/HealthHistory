package infra;

import infra.AdaptadorAnvisa;

/**
 *
 * @author HealthHistory
 */
public class AtualizaCid {
    
    //Singleton
    private static final AdaptadorAnvisa CID10ATUALIZAR = new AdaptadorAnvisa();
    
    public AtualizaCid(){
        
    }
    
    public void atualizar(){
        System.out.println(CID10ATUALIZAR.getAtualizaçãoAnvisa());
        System.out.println("Operação Realizada com sucesso!!");
    }
    
}
