/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import business.control.Fabrica;
import business.control.UsuarioControle;
import business.model.Enfermeiro;
import business.model.Medico;
import business.model.Paciente;
import business.model.Usuario;
import exceptions.CPFException;
import exceptions.LoginException;
import exceptions.NotAValidNumber;
import exceptions.SenhaException;
import exceptions.TipoSanguineoException;
import java.util.Scanner;

/**
 *
 * @author lumo
 */
public class ManipulaUsuario implements Command{
    
    UsuarioControle usuarioControle;
    Scanner scan = new Scanner(System.in);
    int tipoUsuario;

    public ManipulaUsuario(int tipoUsuario) {
        usuarioControle = Fabrica.Criar(usuarioControle, tipoUsuario);
        this.tipoUsuario = tipoUsuario;
        
        
    }

    @Override
    public void execute() {
        int opcao = mostraMenu();  
        
        switch(opcao){
            
            case 1:
        
                try {
                    adicionaUsuario(tipoUsuario);
                } 
                catch (NotAValidNumber ex) {
                    System.err.println(ex.getMessage());
                }
        
                break;
         
            case 2:
                removeUsuario();
                break;
                
            case 3:
                buscaUsuario();
                break;
               
            case 4:
                listaUsuarios();
                break;
                
            case 5:
                return;
                
            default:
        {
            try {
                throw new NotAValidNumber();
            } catch (NotAValidNumber ex) {
                System.err.println(ex.getMessage());
            }
        }
                
        }
    }
    
    
    
    
        
    public void adicionaUsuario(int tipoDeUsuario) throws NotAValidNumber{
        System.out.println("Criando Usuario...");
        String login;
        String senha;
        String nome;
        String sobrenome;
        String cpf;
        String dataNascimento;
        String email;
        String tipoSanguineo;
        String telefone;
        boolean loopFormulario = true;
        
        while(loopFormulario){
            System.out.println("\n\nDigite seu login");
            login = scan.next();
            try {
                usuarioControle.validaLogin(login);
            } catch (LoginException ex) {
                System.err.println(ex.getMessage());
                continue;
            }

            System.out.println("\n\nDigite sua senha");
            senha = scan.next();
            try {
                usuarioControle.validaSenha(senha);
           
            } catch (SenhaException ex) {
                 System.err.println(ex.getMessage());
                continue;
            }
            
            System.out.println("\n\nDigite seu nome");
            nome = scan.next();

            System.out.println("\n\nDigite seu sobrenome");
            sobrenome = scan.next();

            System.out.println("\n\nDigite seu cpf");
            cpf = scan.next();
            
            try {
                usuarioControle.validaCPF(cpf);
            } catch (CPFException ex) {
                System.err.println(ex.getMessage());
                continue;
            }
                
            System.out.println("\n\nDigite sua data de nascimento");
            dataNascimento = scan.next();

            System.out.println("\n\nDigite seu email");
            email = scan.next();

            System.out.println("\n\nDigite seu telefone de contato");
            telefone = scan.next();

            System.out.println("\n\nDigite seu tipo sanguineo");
            tipoSanguineo = scan.next();
            try {
                usuarioControle.validaTipoSanguineo(tipoSanguineo);
            } catch (TipoSanguineoException ex) {
                System.err.println(ex.getMessage());
                continue;
            }
   
            Usuario user;
            switch(tipoDeUsuario){                      //melhorar isso
                case 1: 
                    System.out.println("\n\nDigite uma doença que você possui");
                    String doenca = scan.next();
                    user = new Paciente(cpf,login, senha, nome, sobrenome
                    , dataNascimento, email, telefone, tipoSanguineo, doenca);
                    break;
                    
                case 2:
                    System.out.println("\n\nDigite seu crm");
                    String crm = scan.next();
                    System.out.println("\n\nDigite sua especialidade");
                    String especialidade = scan.next();
                    user = new Medico(cpf, login, senha, nome, sobrenome
                        , dataNascimento, email, telefone, crm, especialidade);
                    break;
                     
                case 3:
                    System.out.println("\n\nDigite seu cre");
                    int cre = scan.nextInt();
                    user = new Enfermeiro(cpf, login, senha, nome, sobrenome
                        , dataNascimento, email, telefone, cre);
                    break;
                
                default:
                    throw new NotAValidNumber();
            }

            
            
            usuarioControle.adiciona(login, user);
            loopFormulario = false;
        }
        
        System.out.println("\n Parabéns! Conta criada com sucesso. \n");
    }
    
    public void removeUsuario(){
        System.out.println("Insira o cpf do usuario que deseja remover: ");
        String cpf = scan.next();
        usuarioControle.remover(cpf);
    }
    
    public void listaUsuarios(){
        System.out.println("\n"+usuarioControle.toString());
    }
    
    public void buscaUsuario(){
        System.out.println("\nDigite o cpf do usuario que deseja buscar");          //mudar isso pra nome
        String cpf = scan.next();
        usuarioControle.buscarIndividual(cpf);
    }
    
    public int mostraMenu(){
        
        System.out.println("\n"
            +          "Adicionar usuário - 1 \n"
            +          "Remove usuário - 2 \n"
            +          "Buscar usuário - 3 \n"
            +          "Listar todos os usuários cadastrados - 4\n"
            +          "Sair - 5 \n");
        return scan.nextInt();
        
    }
}
