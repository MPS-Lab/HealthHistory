/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import business.gerencia.RelatorioPaciente;
import business.gerencia.RelatorioTemplate;

/**
 *
 * @author lumo
 */
class GeraRelatorioPaciente implements Command {

    public GeraRelatorioPaciente() {
    }

    @Override
    public void execute() {
        RelatorioTemplate relatorio = new RelatorioPaciente();
        System.out.println(relatorio.gerarRelatorio("Paciente"));
    }
    
    
    
}
