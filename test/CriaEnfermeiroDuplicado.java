
import business.control.EnfermeiroControle;
import business.model.Enfermeiro;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class CriaEnfermeiroDuplicado {
     
    private static EnfermeiroControle enfermeiroControle;
    private static Enfermeiro enfermeiro;
    
    @BeforeClass
    public static void setUpClass() {
        
        enfermeiroControle = new EnfermeiroControle();
        
        enfermeiro = new Enfermeiro("45879658950", "alissonenfermeiro", "password123", "Alisson", "Galiza",
                "17/05/1996", "alissonenfermeiro@alisson.com", "86998745632", 123);
        
        enfermeiroControle.remover("45879658950");                                 
        enfermeiroControle.adiciona(enfermeiro.getLogin(), enfermeiro);
    }
    
    @Test
    public void criaEnfermeiroDuplicado(){
        Assert.assertFalse(enfermeiroControle.adiciona(enfermeiro.getLogin(), enfermeiro));
        
    }
    
    @After
    public void tearDown(){
        enfermeiroControle.remover("45879658950");
    }
    
}
