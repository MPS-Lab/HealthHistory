package business.control;

import business.control.EnfermeiroControle;
import business.control.MedicoControle;
import business.control.PacienteControle;
import business.control.UsuarioControle;

/**
 *
 * @author lumo
 */
public class Fabrica {
    
    public static UsuarioControle Criar(UsuarioControle uc, int tipoDeUsuario){
        switch(tipoDeUsuario){
            case 1:
                uc = new PacienteControle();
                break;
                
            case 2:
                uc = new MedicoControle();
                break;
                
            case 3:
                uc = new EnfermeiroControle();
                break;
                
        }
        return uc;
    }
}
