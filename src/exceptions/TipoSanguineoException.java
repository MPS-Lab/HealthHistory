package exceptions;

/**
 *
 * @author HealthHistory
 */
public class TipoSanguineoException extends Exception{
    
    public TipoSanguineoException(){
        super("Tipo Sanguineo deve conter até 3 caracteres!!");
    }
    
    public TipoSanguineoException(String exception){
        super(exception);
    }
    
}
